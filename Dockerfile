FROM ruby:2.6.8-alpine3.13

EXPOSE 3000 

WORKDIR /usr/src/app

RUN apk update &&\
    apk add --no-cache git build-base gdbm-dev nodejs postgresql-dev postgresql-client tzdata libxslt-dev libxml2-dev shared-mime-info sqlite-dev yarn &&\
    gem update &&\
    gem install rails -v 6.1.3.2

CMD sh
